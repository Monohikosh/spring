package com.sbercourses.spring.week6;

import com.sbercourses.spring.week6.dao.BookDao;
import com.sbercourses.spring.week6.dao.UserDao;
import com.sbercourses.spring.week6.model.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

@SpringBootApplication
public class Application implements CommandLineRunner {

    private final BookDao bookDao;
    private final UserDao userDao;

    public Application(BookDao bookDao, UserDao userDao) {
        this.bookDao = bookDao;
        this.userDao = userDao;
    }

//    @Autowired
//    public void setBookDao(BookDao bookDao) {
//        this.bookDao = bookDao;
//    }

//    @Autowired
//    public Application(@Autowired BookDao bookDao) {
//        this.bookDao = bookDao;
//    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        System.out.println(bookDao.findById(2L));
//        System.out.println(bookDao.findAll());
        userDao.addUser(new User(
                "petrov",
                "eugene",
                new Date(33333L),
                79950053575L,
                "eee@e.ru",
                "Сестра моя - жизнь, Путешествие из Петербурга в Москву"
        ));

        userDao.addUser(new User(
                "ivanov",
                "petr",
                new Date(88888888L),
                79008007060L,
                "iii@i.ru",
                "Доктор Живаго, Недоросль"
        ));

        userDao.addUser(new User(
                "petrov",
                "ivan",
                new Date(),
                79539900910L,
                "ppp@p.ru",
                "Python, Java, Spring in action"
        ));
        for (String title : userDao.findAllBooksByUser("ppp@p.ru")) {
            System.out.println(bookDao.findBookByTitle(title));
        }
    }
}
