package com.sbercourses.spring.week6.model;

import lombok.*;

import java.util.Date;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User {

    private String surname;
    private String name;
    private Date birthday;
    private Long phoneNumber;
    private String email;
    private String books;

}
