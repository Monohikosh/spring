package com.sbercourses.spring.week6.model;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
// ORM - Object Relational Mapping
// (объектно-реляционное отображение)
public class Book {

    private Long id;
    private String title;
    private String author;
    private Date addedDate;

}
