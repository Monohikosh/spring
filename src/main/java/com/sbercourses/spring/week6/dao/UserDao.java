package com.sbercourses.spring.week6.dao;

import com.sbercourses.spring.week6.model.User;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserDao {

    private final Connection connection;

    public UserDao(Connection connection) {
        this.connection = connection;
    }


    public void addUser(User user) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "insert into users(surname, name, birthday, phone_number, email, list_books)" +
                        "values (?, ?, ?, ?, ?, ?)");
        preparedStatement.setString(1, user.getSurname());
        preparedStatement.setString(2, user.getName());
        preparedStatement.setTimestamp(3, new Timestamp(user.getBirthday().getTime()));
        preparedStatement.setLong(4, user.getPhoneNumber());
        preparedStatement.setString(5, user.getEmail());
        preparedStatement.setString(6, user.getBooks());
        preparedStatement.executeUpdate();
    }

    public String[] findAllBooksByUser(String email) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
               "select list_books from users " +
                        "where email = ?");
        preparedStatement.setString(1, email);
        ResultSet resultSet = preparedStatement.executeQuery();
        String[] array = new String[] {};
        while (resultSet.next()) {
            array = resultSet.getString("list_books").split(", ");
        }
        return array;
    }
}
